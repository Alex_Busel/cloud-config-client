package alexb.demo.config.client.props;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

//@Data
//@AllArgsConstructor
//@Builder
// todo -> gradle 5 + lombok + IDEA
public class HelloPropertiesDto {
    private String message;
    private String source;
    private int number;

    public HelloPropertiesDto(String message) {
        this.message = message;
    }

    public HelloPropertiesDto(String message, String source, int number) {
        this.message = message;
        this.source = source;
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public String getSource() {
        return source;
    }

    public int getNumber() {
        return number;
    }
}
