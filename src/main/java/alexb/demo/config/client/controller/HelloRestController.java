package alexb.demo.config.client.controller;


import alexb.demo.config.client.props.HelloProperties;
import alexb.demo.config.client.props.HelloPropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloRestController {
    @Autowired
    private HelloProperties helloProperties;

    @RequestMapping(method = RequestMethod.GET)
    public HelloPropertiesDto getHello() {
        return new HelloPropertiesDto(helloProperties.getMessage(), helloProperties.getSource(),
            helloProperties.getNumber());
    }
}
